/**
 * @license
 * Copyright 2020 İlteriş Yağıztegin Eroğlu (linuxgemini)
 * SPDX-License-Identifier: MIT
 */

"use strict";

const PORT = (process.env.PORT || 3000);

const express = require("express");
const geolite2 = require("geolite2-redist");
const maxmind = require("maxmind");
const app = express();

app.disable("x-powered-by");
app.enable("trust proxy");

const dataSpill = async (req) => {
    let ip = req.ip.replace(/^::ffff:/g, "");
    let useragent = req.header('User-Agent') || "";
    let requestHost = req.hostname;

    let ccLookup = await geolite2.open("GeoLite2-Country", path => {
        return maxmind.open(path);
    });
    let asnLookup = await geolite2.open("GeoLite2-ASN", path => {
        return maxmind.open(path);
    });

    let asnLookupResult = asnLookup.get(ip);
    let countryLookupResult = ccLookup.get(ip);

    let asn = (asnLookupResult.autonomous_system_number ? asnLookupResult.autonomous_system_number : "");
    let country = (countryLookupResult.country ? countryLookupResult.country.iso_code : "");

    ccLookup.close();
    asnLookup.close();

    return {
        ip,
        asn,
        country,
        useragent,
        requestHost
    };
};

const handleOnDefault = (data) => {
    let res = [];

    // for the ops who has to curl, salute
    res.push("");

    res.push(`Client IP:          ${data.ip}`);
    res.push(`Client ASN:         ${data.asn}`);
    res.push(`Client Country:     ${data.country}`);

    res.push("");

    res.push(`Client User-Agent:  ${data.useragent}`);
    res.push(`Request Hostname:   ${data.requestHost}`);

    // paying respects to all curlers again
    res.push("\n");

    return res.join("\n");
};

const handleOnJSON = (data) => {
    let previz = JSON.stringify(data, null, 4);
    return `${previz}\n`
};

const handleOnPlain = (data) => {
    let res = [];

    res.push(data.ip);
    if (data.asn !== "") res.push(`AS${data.asn}`);
    if (data.country !== "") res.push(data.country);
    res.push("");

    return res.join("\n");
};

app.all("/plain", async (req, res, next) => {
    let obj = await dataSpill(req);
    let resp = handleOnPlain(obj);

    res.set({
        "Content-Type": "text/plain;charset=UTF-8"
    });
    return res.status(200).send(resp);
});

app.all("/json", async (req, res, next) => {
    let obj = await dataSpill(req);
    let resp = handleOnJSON(obj);

    res.set({
        "Content-Type": "application/json;charset=UTF-8"
    });
    return res.status(200).send(resp);
});

app.all("/ip", async (req, res, next) => {
    let obj = await dataSpill(req);
    let resp = `${obj.ip}\n`;

    res.set({
        "Content-Type": "text/plain;charset=UTF-8"
    });
    return res.status(200).send(resp);
});

app.all("/", async (req, res, next) => {
    let obj = await dataSpill(req);
    let resp;

    if (obj.useragent.startsWith("curl/")) {
        resp = `${obj.ip}\n`;
    } else {
        resp = handleOnDefault(obj);
    }

    res.set({
        "Content-Type": "text/plain;charset=UTF-8"
    });
    return res.status(200).send(resp);
});

app.all("*", async (req, res, next) => {
    let obj = await dataSpill(req);
    let resp = handleOnDefault(obj);

    res.set({
        "Content-Type": "text/plain;charset=UTF-8"
    });
    return res.status(200).send(resp);
});

(async () => {
    console.log("geolite2 db download init");
    await geolite2.downloadDbs();
})();

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}...`);
});
